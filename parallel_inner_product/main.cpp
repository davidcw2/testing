#include <algorithm>
#include <cassert>
#include <iostream>
#include <iterator>
#include <random>
#include <string.h>
#include <vector>

#include <omp.h>
#include <sys/time.h>

#include <stapl/views/vector_view.hpp>
#include <stapl/containers/vector/vector.hpp>
#include <stapl/algorithms/algorithm.hpp>
#include <stapl/algorithms/functional.hpp>
#include <stapl/stream.hpp>
#include <stapl/runtime.hpp>
#include <stapl/skeletons/serial.hpp>

#define SIZE 524288
#define THREADS 12

using namespace std;
typedef stapl::vector<int> vec_int;
typedef stapl::vector_view<vec_int> vec_view_int;

struct msg {
private:
    string m_txt;
public:
    msg(string text) // ## 5
        : m_txt(text)
    { }

    typedef void result_type;

    result_type operator() () {
        cout << m_txt << endl; // ## 6
    }

    void define_type(stapl::typer& t) {
        t.member(m_txt);
    }
};


double get_time() {
    struct timeval tv;
    gettimeofday(&tv, 0);
    return (tv.tv_sec * 1e6 + tv.tv_usec);
}

template <typename T, typename U>
void gen_vector(U result, T size) {
    for (T i = 0; i < size; i++) {
        *result++ = rand() % 100;
    }
}

template <typename T>
void print(const T& v) {
    copy(v.begin(), v.end(), ostream_iterator<typename T::value_type>(cout, " "));
    cout << endl;
}

// Naive inner product
template <typename T>
T naive_inner_product(vector<T>& A, vector<T>& B) {
    assert(A.size() == B.size());
    T result = 0;

    for (int i = 0; i < A.size(); i++) {
        result += A[i] * B[i];
    }

    return result;
}

// Worksharing approach (omp parallel for)
template <typename T>
T workshare_inner_product(vector<T>& A, vector<T>& B) {
    assert(A.size() == B.size());
    T result = 0;
    
    #pragma omp parallel for reduction(+:result)
    for (int i = 0; i < A.size(); i++) {
            result += A[i] * B[i];
    }

    return result;
}

// Segmented approach (each thread sums up a segment)
template <typename T>
T segmented_inner_product(vector<T>& A, vector<T>& B) {
    assert(A.size() == B.size());
    T result = 0;
    T partial;
    
    #pragma omp parallel private(partial) reduction(+:result)
    {
        partial = 0;

        int tid = omp_get_thread_num();
        int n_threads = omp_get_num_threads();
        int N = A.size();
        int low = (tid * N) / n_threads;
        int high = min(N, ((tid + 1) * N) / n_threads);

        for (int i = low; i < high; i++) {
            partial += A[i] * B[i];
        }

        result += partial;
    }

    return result;   
}

// Cyclical approach (each thread sums up at a stride)
template <typename T>
T cyclical_inner_product(vector<T>& A, vector<T>& B) {
    assert(A.size() == B.size());
    T result = 0;
    T partial;
    
    #pragma omp parallel private(partial) reduction(+:result)
    {
        partial = 0;

        int tid = omp_get_thread_num();
        int n_threads = omp_get_num_threads();
        int N = A.size();

        for (int i = tid; i < N; i += n_threads) {
            partial += A[i] * B[i];
        }

        result += partial;
    }

    return result;   
}

void benchmark_inner_product() {
    cout << "Running inner product benchmarks with size " << SIZE << " and " << THREADS << " threads" << endl;
    cout << "----------------------------------------\nNaive (sequential) Inner Product..." << endl;

    vector<int> A;
    vector<int> B;

    gen_vector(back_inserter(A), SIZE);
    gen_vector(back_inserter(B), SIZE);

    double avg_time = 0.0;
    double start;

    for (int i = 0; i < 30; i++) {
        start = get_time();
        naive_inner_product(A, B);
        avg_time += get_time() - start;
    }

    cout << "Average (30 runs) = " << (avg_time / 30.0) << " us" << endl;
    cout << "----------------------------------------\nWorkshare (parallel) Inner Product..." << endl;
    avg_time = 0.0;

    for (int i = 0; i < 30; i++) {
        start = get_time();
        workshare_inner_product(A, B);
        avg_time += get_time() - start;
    }

    cout << "Average (30 runs) = " << (avg_time / 30.0) << " us" << endl;
    cout << "----------------------------------------\nSegmented (parallel) Inner Product..." << endl;
    avg_time = 0.0;

    for (int i = 0; i < 30; i++) {
        start = get_time();
        segmented_inner_product(A, B);
        avg_time += get_time() - start;
    }

    cout << "Average (30 runs) = " << (avg_time / 30.0) << " us" << endl;
    cout << "----------------------------------------\nCyclical (parallel) Inner Product..." << endl;
    avg_time = 0.0;

    for (int i = 0; i < 30; i++) {
        start = get_time();
        cyclical_inner_product(A, B);
        avg_time += get_time() - start;
    }

    cout << "Average (30 runs) = " << (avg_time / 30.0) << " us" << endl;
}

double stapl_inner_product() {
    vec_int A(SIZE);
    vec_view_int A_view(A);
    vec_int B(SIZE);
    vec_view_int B_view(B);

    stapl::generate(A_view, stapl::random_sequence());
    stapl::generate(B_view, stapl::random_sequence());

    double result;
    stapl::inner_product(A_view, B_view, result);

    return result;
}

stapl::exit_code stapl_main(int argc, char **argv) {
    srand(time(nullptr));
    omp_set_num_threads(THREADS);

    benchmark_inner_product();

    double start = get_time();
    stapl_inner_product();
    stapl::do_once(msg("----------------------------------------\nSTAPL Inner Product...\nTime: " + to_string(get_time() - start) + "\n----------------------------------------"));

    return EXIT_SUCCESS;
}