#!/bin/bash
export STAPL_ROOT=/home/davidcw2/stapl_old
export MPICH_CXX=g++-11.2.0
export USER_LIB=-lpthread

STAPL_FLAGS="-D_STAPL -DSTAPL_NDEBUG -DSTAPL_PER_VIEW_LOCALIZATION"
STAPL_INCLUDE_DIRS="-I${STAPL_ROOT}/tools/libstdc++/11.2.0 -I${STAPL_ROOT}/tools -I${STAPL_ROOT}"
STAPL_CXXFLAGS="${STAPL_FLAGS} ${STAPL_INCLUDE_DIRS}"
LINUX_GCCFLAGS="-Wno-unused-local-typedefs -Wno-unknown-pragmas -Wno-misleading-indentation -Wno-deprecated-declarations -std=c++14 -D_GLIBCXX_USE_CXX11_ABI=0"
AUTOFLAGS="-I${BOOST_ROOT}/include -DBOOST_RESULT_OF_USE_TR1_WITH_DECLTYPE_FALLBACK -DSTAPL_RUNTIME_USE_OMP -fopenmp"
AUTOLIB="-L${STAPL_ROOT}/lib -lstapl -lrt -L${BOOST_ROOT}/lib -lboost_serialization -lboost_system -lpthread"

mpic++ ${STAPL_CXXFLAGS} ${LINUX_GCCFLAGS} ${AUTOFLAGS} -c "main.cpp"
mpic++ ${LINUX_GCCFLAGS} ${AUTOFLAGS} "main.o" ${AUTOLIB} -o  "main.exe"
